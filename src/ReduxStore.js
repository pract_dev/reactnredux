import { createStore, combineReducers } from 'redux';
import { reducer as reduxFormReducer,reset } from 'redux-form';

let defaultState = {
    initData:{id:"",firstName:"",lastName:"",email:"",phone:"",sex:""},
    userData:[],
    updateData:{}
};
const userAppReducer = (state = defaultState, action) => {

                    switch(action.type) {
                        case 'ADD_USER':
                            var userData =[];
                            if(state.userData.length !==0){
                                userData = state.userData.filter((user)=>{
                                                    return user.id !== action.data.id && user.id !="";
                                                })
                            }
                            userData.push(action.data);
                        return {...state,userData};

                        case 'LOAD_DATA':
                            return {...state,updateData:{...action.data}}
                        case 'RESET_FORM':   
                           return {...state,updateData:{}}
                        default:
                        return state;
                    }
}
const reducer = combineReducers({
                form: reduxFormReducer,
                userAppReducer
});

const store = (window.devToolsExtension
  ? window.devToolsExtension()(createStore)
  : createStore)(reducer);

export default store;