import React from 'react';
import {Edit} from '@material-ui/icons';
import MUIDataTable from "mui-datatables";
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
class  UserSummartTable extends React.Component {
  
   constructor(props){
                super(props)
                this.handleData = this.handleData.bind(this);
                this.handleEdit = this.handleEdit.bind(this);
                this.handleClick = this.handleClick.bind(this);
   }
   componentDidMount(){
           console.log("component mounted");
   }
   handleClick(){
        this.props.dispatch({
                type:'RESET_FORM',
          });
          this.props.history.push("/user");
   }
   handleEdit(value,rowData){
           console.log('rowData-',rowData);
            const userObj = {};
            userObj.firstName=rowData[0];
            userObj.lastName=rowData[1];
            userObj.email=rowData[2];
            userObj.phone=rowData[3];
            userObj.sex=rowData[4];
            userObj.id=rowData[5];
            console.log('userObj-',userObj);
            this.props.dispatch({
                type:'LOAD_DATA',
                data:userObj
            });
            this.props.history.push("/user");
   }
  handleData(){
        let data=[];
        let usersDataCol = this.props.users.userAppReducer;
        console.log('usersData--',usersDataCol);
        if(usersDataCol.length !== 0){
                let userFilterData = usersDataCol.userData.filter((user)=>{
                        return user.id !=""
                });
               data= userFilterData.map((user)=>{
                       return [user.firstName,user.lastName,user.email,user.phone,user.sex,user.id]
                });
          }
          console.log("table data--",data);
          return data;
  }
 render(){

    const columns = ["First Name", "Last Name", "Email", "Phone","Gender",{
        name: "Action",
        options: { 
            filter: false,
            download: false,
            sort:false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <div>  
                        <Edit onClick={()=>this.handleEdit(value,tableMeta.rowData)} style={{cursor:"pointer"}}/>
                    </div>
                );
              },         
        }}];
    
     const options = {
        filterType: 'checkbox',
        selectableRows:false
      };

        return(
                <div>
                <Button variant="contained" color="primary" onClick ={this.handleClick}>
                                Create User
                </Button>
                <MUIDataTable 
                title={"UserList"}
                data={this.handleData()}
                columns={columns}
                options={options}
        />
           </div>

        )
    }
}

const mapStateToProps = (state) => {
        return {
                users: state
         }
     }
 
export default connect(mapStateToProps)(UserSummartTable);




