import React from "react";
import { Field, reduxForm } from "redux-form";
import {connect} from 'react-redux';


let UserForm = props => {
 
 const { handleSubmit, pristine, reset, submitting } = props;
 
  return (
    <form onSubmit={handleSubmit}>
       
       <div>
        <label>First Name</label>
        <div>
          <Field
            name="firstName"
            component="input"
            type="text"
            placeholder="First Name"
          />
        </div>
      </div>
      <div>
        <label>Last Name</label>
        <div>
          <Field
            name="lastName"
            component="input"
            type="text"
            placeholder="Last Name"
          />
        </div>
      </div>
      <div>
        <label>Email</label>
        <div>
          <Field
            name="email"
            component="input"
            type="email"
            placeholder="Email"
          />
        </div>
      </div>
      <div>
        <label>Phone</label>
        <div>
          <Field
            name="phone"
            component="input"
            type="number"
            placeholder="Phone"
          />
        </div>
      </div>
      <div>
        <label>Gender</label>
        <div>
          <label>
            <Field name="sex" component="input" type="radio" value="male" />{" "}
            Male
          </label>
          <label>
            <Field name="sex" component="input" type="radio" value="female" />{" "}
            Female
          </label>
        </div>
      </div>
       <div>
        <button type="submit" disabled={submitting} style={{margin:"5px"}}>
          Submit
        </button>
        
      </div>
    </form>
  );
};

UserForm = reduxForm({
                form: "simple",// a unique identifier for this form
            })(UserForm);

UserForm = connect(
    (state) =>{
          let userUpdate = state.userAppReducer.updateData;
          return Object.keys(userUpdate).length ==0? 
                {initialValues :state.userAppReducer.initData} : 
                {initialValues :state.userAppReducer.updateData}
    }
)(UserForm)

export default UserForm;