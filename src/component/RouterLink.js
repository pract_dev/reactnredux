import React from 'react';
import { BrowserRouter as Router ,Link,Route } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import SummaryIcon from '@material-ui/icons/PeopleAlt';
import AddUserIcon from '@material-ui/icons/PersonAdd';
import {connect} from 'react-redux';


class RouterLink extends React.Component{

    handleClick = ()=>{
        this.props.dispatch({
            type:'RESET_FORM',
      });
        
    }

    render(){
        return(
      
            <List>
                 <ListItem button key="User Summary" onClick={this.handleClick} component={Link} to={"/"}>
                     <ListItemIcon> <SummaryIcon /> </ListItemIcon>
                     <ListItemText>User Summary</ListItemText>
                 </ListItem>
 
                 <ListItem button key="Create User" onClick={this.handleClick}  component={Link} to={"/user"} >
                     <ListItemIcon> <AddUserIcon /> </ListItemIcon>
                     <ListItemText > User</ListItemText>
                 </ListItem>
         </List>
     
   )
    }
    
}

export default connect()(RouterLink);