import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { useStyles } from './UserAppStyles';
import  Drawer from './Drawer';
import Contents from './Content';
import HeaderBar from './HeaderBar';


export default function HeaderComp(){

    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
  
    function handleDrawerOpen() {
      setOpen(true);
    }
  
    function handleDrawerClose() {
      setOpen(false);
    }
  
    return (
      
      <div className={classes.root}>
         <CssBaseline />
        <HeaderBar  classes={classes} open={open} handleDrawerOpen = {handleDrawerOpen} ></HeaderBar>
        <Drawer handleDrawerClose = {handleDrawerClose} classes = {classes} theme ={theme} open={open}></Drawer>
        <Contents classes={classes} open={open}></Contents>
      </div>
      
    );
}