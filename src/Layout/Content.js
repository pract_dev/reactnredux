import React from 'react';
import clsx from 'clsx';
import { BrowserRouter as Router,Route,Switch  } from 'react-router-dom';
import UserSummaryTable from '../component/MUITable';
import UserFormComp from '../container/ReduxFormCont';
export default function Contents(props){

    const {classes,open} =props;
   
   return (
     
      <main 
         className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
      <div className={classes.drawerHeader} />
              <Switch>
                  <Route exact path="/" component={UserSummaryTable} />
                  <Route path="/user" component={UserFormComp} />
              </Switch>
     </main>
  
   )
}