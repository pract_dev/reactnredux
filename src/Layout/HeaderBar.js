import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import clsx from 'clsx';

export default function HeaderBar(props){
   
   const {classes ,handleDrawerOpen,open } = props;
    return(
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
          <Grid
              justify="space-between" // Add it here :)
              container 
              spacing={24}
        >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Grid item>
            <Typography variant="h6" noWrap>
             GIS
            </Typography>
            </Grid>
            <Grid item>
            <Chip
                      className={classes.UserChip}
                      avatar={
                        <Avatar className={classes.UserAvatar}>
                          {"G"}
                        </Avatar>
                      }
                      label={"Guest"}
                      variant="outlined" 
                  />
                  </Grid>
          </Grid>
          </Toolbar>
       </AppBar>
    )
    
}