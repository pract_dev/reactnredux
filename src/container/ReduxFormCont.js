import React from 'react';
import UserForm from '../component/ReduxForm';
import {connect} from 'react-redux';
import { reset } from 'redux-form';
import { Redirect } from 'react-router-dom'

class FormContainer extends React.Component {

   hamdleSubmit = values =>{
        console.log("values--",values);
       if(values.id ==""){
            values.id= Math.floor((Math.random() * 1000) + 1);
       }
       
       this.props.dispatch({
           type:'ADD_USER',
           data:values
       });
      
       this.props.dispatch(reset('simple'))
       this.props.history.push("/")
    }

    render(){
       
        return ( 
            <div>
                <UserForm onSubmit={this.hamdleSubmit}></UserForm>
            </div> 
        )
    }
    
}

const mapStateToProps = (state) => {
        return {
                users: state
         }
     }
export default connect(mapStateToProps)(FormContainer);