import React from 'react';
import './App.css';
import Layout from './Layout/Layout';
import { Provider } from "react-redux";
import { BrowserRouter as Router} from 'react-router-dom';
import ReduxStore from './ReduxStore';


function App() {
  return (
       <Provider store={ReduxStore}>
       <Router>
          <div className="App">
              <Layout></Layout>
          </div>
        </Router>
        </Provider>
  );
}

export default App;
